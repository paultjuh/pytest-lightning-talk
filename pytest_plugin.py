import subprocess
import tempfile
import os
import sys
import re

from IPython.core.magic import register_cell_magic


def load_ipython_extension(ipython):
    register_cell_magic(pytest)


def unload_ipython_extension(ipython):
    pass


def createfile(*path, contents='\n'):
    with open(os.path.join(*path), 'w') as fo:
        fo.write(contents)
        

def pytest(line, cell):
    with tempfile.TemporaryDirectory() as tmpdir:
        createfile(tmpdir, 'pytest.ini')
        createfile(tmpdir, 'test_file.py', contents=HEADER.format(cell))
        args = tuple(i for i in line.split(' ') if i)
        try:
            lines = int(args[0])
            args = args[1:]
        except Exception:
            lines = 8
        process = subprocess.Popen(('py.test', '--color=yes', '-v') + args,
                                   cwd=tmpdir, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        stdout, stderr = process.communicate()
        stdout = stdout.replace(b'\x1b[1m\x1b[31m', b'\x1b[1;38;2;255;0;0m')  # make read clearer
        stdout = re.sub(rb''.join([rb'^(.*\n){', str(lines).encode('ascii'), rb'}']), b'', stdout)
        stdout = stdout.strip()
        sys.stdout.write(stdout)
        sys.stdout.flush()

HEADER = '''import pytest


def div(a, b):
    return a / b


{}
'''
